import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as firebase from 'firebase';
import 'firebase/firestore';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFirestoreModule} from 'angularfire2/firestore';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class CoreModule { }
