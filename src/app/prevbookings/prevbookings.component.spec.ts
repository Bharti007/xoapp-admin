import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrevbookingsComponent } from './prevbookings.component';

describe('PrevbookingsComponent', () => {
  let component: PrevbookingsComponent;
  let fixture: ComponentFixture<PrevbookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrevbookingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrevbookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
