import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
@Component({
  selector: 'app-prevbookings',
  templateUrl: './prevbookings.component.html',
  styleUrls: ['./prevbookings.component.css']
})
export class PrevbookingsComponent  {
    public users:any=[]
    constructor(public auth: AuthService) {
      this.auth.getAppBookings('prev').then(resp => {
              console.log(resp)
              if(resp ){
                  this.users = resp;

              }
          },
          error => {
              this.auth.showToastr('error', "Error", "Something Went wrong")
          }
      );
  }

  ngOnInit() {
  }

}
