import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BookingsComponent} from './bookings/bookings.component';
import {PrevbookingsComponent} from './prevbookings/prevbookings.component';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'pages',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      // {
      //   path: 'base',
      //   loadChildren: './views/base/base.module#BaseModule'
      // },
      // {
      //   path: 'buttons',
      //   loadChildren: './views/buttons/buttons.module#ButtonsModule'
      // },
      // {
      //   path: 'charts',
      //   loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
      // },
      {
        path: 'service-provider',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      // {
      //   path: 'editors',
      //   loadChildren: './views/editors/editors.module#EditorsModule'
      // },
      // {
      //   path: 'forms',
      //   loadChildren: './views/forms/forms.module#FormsModule'
      // },
      // {
      //   path: 'google-maps',
      //   loadChildren: './views/google-maps/google-maps.module#GoogleMapsModule'
      // },
      // {
      //   path: 'icons',
      //   loadChildren: './views/icons/icons.module#IconsModule'
      // },
      // {
      //   path: 'notifications',
      //   loadChildren: './views/notifications/notifications.module#NotificationsModule'
      // },
      // {
      //   path: 'plugins',
      //   loadChildren: './views/plugins/plugins.module#PluginsModule'
      // },
      // {
      //   path: 'tables',
      //   loadChildren: './views/tables/tables.module#TablesModule'
      // },
      {
        path: 'users',
        loadChildren: './views/users/users.module#UsersModule'
      },
        {
            path: 'service',
            loadChildren: './views/service/service.module#ServiceModule'
        },
       {
        path: 'events',
        loadChildren: './views/events/events.module#EventsModule'
      },

        {
            path: 'bookings',
            component: BookingsComponent,
            data: {
                title: 'Booking'
            }
        },
        {
            path: 'prevbookings',
            component: PrevbookingsComponent,
            data: {
                title: 'Previous Booking'
            }
        }
      // {
      //   path: 'theme',
      //   loadChildren: './views/theme/theme.module#ThemeModule'
      // },
      // {
      //   path: 'uikits',
      //   loadChildren: './views/uikits/uikits.module#UIKitsModule'
      // },
      // {
      //   path: 'widgets',
      //   loadChildren: './views/widgets/widgets.module#WidgetsModule'
      // }
    ]
  },
  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './views/pages/pages.module#PagesModule',
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
