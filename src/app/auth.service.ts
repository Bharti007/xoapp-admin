import {Injectable, ViewEncapsulation} from '@angular/core';
import * as firebase from 'firebase';
import 'firebase/firestore';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFirestoreModule, AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/observable';
import {Router, RouterModule} from '@angular/router';

import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';

import {ToasterModule, ToasterService, ToasterConfig} from 'angular2-toaster/angular2-toaster';
import {environment} from '../environments/environment';
import {forEach} from '@angular/router/src/utils/collection';

interface posts {
    title: string,
    description: string,
    image: string,
    createdOn: string,
}


@Injectable()
export class AuthService {
    postcollection: AngularFirestoreCollection<posts>;
    private toasterService: ToasterService;
    private firestoreDB = firebase.firestore();
    postData: any;
    users: any = [];
    public toasterconfig: ToasterConfig =
        new ToasterConfig({
            tapToDismiss: true,
            timeout: 5000
        });

    constructor(toasterService: ToasterService, public afs: AngularFireDatabase, public afstore: AngularFirestore, public http: HttpClient, public router: Router) {
        this.toasterService = toasterService;
        this.postcollection = this.afstore.collection('posts');
        firebase.auth().onAuthStateChanged((user) => {
            console.log(user);
            if (user) {
                if (user['isAnonymous']) {
                    //Goto Trial Page.
                    // this.navCtrl.setRoot(Login.trialPage, { animate: false });
                } else {

                }
            }
        });

        
    }

    // ngOnInit() {
    //
    // }


    /*
       This method is to register admin
       Author: Bharti
    */
    registerUser(email, password) {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((success) => {
                this.showToastr('success', '', 'User registered successfully')

                let user = firebase.auth().currentUser;
                let dateCreated = new Date();
                this.emailLogin(email, password);
                // firebase.database().ref('accounts/'+user.uid).set({
                //   createdon:dateCreated,
                //   username: this.emailPasswordForm.value["username"],
                //   name: this.emailPasswordForm.value["fullname"],
                //   userId:user.uid,
                //   email:user.email,
                //   provider:"Email",
                //   img:this.img
                // });
            })
            .catch((error) => {
                this.showToastr('error', 'Register Error', error.message)
                let code = error['code'];
            });
    }

    /*
      This method is to login admin
      Author: Bharti
   */

    emailLogin(email, password) {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((success) => {
                this.showToastr('success', '', 'Logged in successfully')
                console.log('succes', success);
                this.router.navigate(['/users']);

            })
            .catch((error) => {
                this.showToastr('error', 'Login Error', error.message)
                let code = error['code'];
            });
    }

    /*
      This method is to show toast
      Author: Bharti
   */

    showToastr(type, title, content) {
        var toast = {
            type: type,
            title: title,
            body: content
        };
        this.toasterService.pop(toast);
    }

    /*
      This method is to get all the users of the app
      Author: Bharti
   */

    getAppUsers(type) {
        var promise = new Promise((resolve, reject) => {
            this.firestoreDB.collection('users').onSnapshot((querySnapshot)=>{
                let arr = [];
                querySnapshot.forEach(function (doc) {
                    let obj = JSON.parse(JSON.stringify(doc.data()));
                    if(obj.type === type){
                        obj.$key = doc.id;
                        arr.push(obj);
                    }
                });
                console.log(arr);
                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                } else {
                    console.log("No such document!");
                    reject(null);
                }

            });
        });
        return promise;
    }


    getAppBookings(type) {
        var promise = new Promise((resolve, reject) => {
            this.firestoreDB.collection('bookings').onSnapshot((querySnapshot)=>{
                let arr = [];
                querySnapshot.forEach(function (doc) {
                    let obj = JSON.parse(JSON.stringify(doc.data()));
                    if(type === 'all'){
                        if(obj.status !== 'Cancelled' && obj.status !== 'Completed' && obj.status !== 'Ended'){
                            obj.$key = doc.id;
                            arr.push(obj);
                        }

                    }
                    else{
                        if(obj.status === 'Cancelled' || obj.status === 'Completed' || obj.status === 'Ended'){
                            obj.$key = doc.id;
                            arr.push(obj);
                        }
                    }
                });
                console.log(arr);
                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                } else {
                    console.log("No such document!");
                    reject(null);
                }

            });
        });
        return promise;
    }

    getPosts() {
        var promise = new Promise((resolve, reject) => {
            // this.postcollection.valueChanges()
            this.postcollection.snapshotChanges()
                .map(actions => {
                    return actions.map(a => {
                        const data = a.payload.doc.data() as posts;
                        data['id'] = a.payload.doc.id;
                        return {data};
                    });
                })
                .subscribe(data => {
                        resolve(data);
                    },
                    error => {
                        this.showToastr('error', '', 'Something went wrong.Please try again later.');
                        reject(error);
                    });
        });
        return promise;

    }

    addPost(postData) {
        const promise = new Promise((resolve, reject) => {
            this.firestoreDB.collection('posts')
                .add({
                    'title': postData.title,
                    'description': postData.description,
                    'image': postData.description,
                    'createdOn': new Date()
                })
                .then((resp) => {
                    this.showToastr('success', '', 'Post added successfully.')
                    resolve(resp);
                    this.getAppUsers("").then(resp1 => {
                            if (resp1) {
                                // let users =[];
                                this.users = resp1;

                                this.users.forEach(key => {
                                    if (key.hasOwnProperty('pushToken')) {
                                        this.sendPushNotification( postData.title, postData.description, key.pushToken)
                                            .subscribe(data => {console.log(data)});
                                    }
                                    console.log(key);
                                });


                            }
                        },
                        error => {
                            this.showToastr('error', 'Error', 'Something Went wrong')
                        }
                    );


                }).catch((error) => {
                this.showToastr('error', '', 'Something went wrong.Please try again later.')
                reject(error);
            });
        });
        return promise;
    }

    updatePost(postData) {
        this.postcollection.doc(postData.id).update(postData).then(resp => {
                this.showToastr('success', '', 'Post updated successfully.')
            },
            error => {
                this.showToastr('error', '', 'Something went wrong.Please try again later.')

            }
        );
    }

    updateUser(userData,id) {
        var usercollection = this.afstore.collection('users');
        usercollection.doc(id).update(userData).then(resp => {
                //this.showToastr('success', '', 'Post updated successfully.')
            },
            error => {
                this.showToastr('error', '', 'Something went wrong.Please try again later.')

            }
        );
    }



    deletePost(postId) {
        this.postcollection.doc(postId).delete().then(resp => {
                this.showToastr('success', '', 'Post deleted successfully.');
            },
            error => {
                this.showToastr('error', '', 'Something went wrong.Please try again later.')
            }
        );
    }

   // sendPushNotification(title, msg, token) {
        // console.log(token)
        // const body = {
        //     'to': token,
        //     'notification': {
        //         'text': msg,
        //         'title': title,
        //         'click_action': 'OPEN_ACTIVITY_1'
        //     },
        //     'data': 'any value'
        // }
        // const headers = new Headers({
        //     'Content-Type': 'application/json',
        //     'Authorization': 'key=AAAA-f3mWsE:APA91bEP4oLh_QKJFNFrr5D199vAAZICcbyR8wErw9ZHUxtDf-wi569QaTg1qIj1peqDa-8TZOuAWFa1QC9OWtyZSAxj3IS6HiAmw-h-RJVS6XjvpkxmgJ46nMiHVHa4h3P73D_2BYwc'
        // });
        // const options = new RequestOptions({headers: headers});
        // return this.http.post('https://fcm.googleapis.com/fcm/send', body, options)
        //     .map((res) => {
        //             console.log(res)
        //             res.json()
        //         }
        //     )
        //     .catch((error: any) => {


        //         console.log(error);

        //         return Observable.throw(error.statusText);
        //     });
   // }

    public sendPushNotification(title, msg, token) {

      const data = {  'notification': {
                'title': title,
                'text': msg,
                'click_action': 'OPEN_ACTIVITY_1'
            },
            'data': {
                'keyname': 'any value '
            },
            'to': token
      }
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAA-f3mWsE:APA91bEP4oLh_QKJFNFrr5D199vAAZICcbyR8wErw9ZHUxtDf-wi569QaTg1qIj1peqDa-8TZOuAWFa1QC9OWtyZSAxj3IS6HiAmw-h-RJVS6XjvpkxmgJ46nMiHVHa4h3P73D_2BYwc'
        });
        return this.http.post('https://fcm.googleapis.com/fcm/send', data, {
            headers: headers,
            observe: 'response'
        }).map(resp => {
            return resp.body;
        });
    }


}
