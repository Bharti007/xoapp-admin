import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent  {
public users:any=[]
  constructor(public auth: AuthService) {
      this.auth.getAppBookings('all').then(resp => {
              console.log(resp)
              if(resp ){
                  this.users = resp;

              }
          },
          error => {
              this.auth.showToastr('error', "Error", "Something Went wrong")
          }
      );
  }

  ngOnInit() {
  }

}
