import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsComponent } from './events.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
      data: {
      title: 'Posts'
    },
     component: EventsComponent,
  
       
    
    children: [
      {
        path: 'edit/:id',
        loadChildren: './edit-event/edit-event.module#EditEventModule'   
      },
      
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
