import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../auth.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent {
    users:any=[]
    constructor(public auth: AuthService) {
        this.getUser();

    }

    getUser(){
        this.auth.getAppUsers('serviceprovider').then(resp => {
                if(resp ){
                    this.users = resp;
                }
            },
            error => {
                this.auth.showToastr('error', "Error", "Something Went wrong")
            }
        );
    }

    changeStatus(status,id){
        var data = {'isActive': status};
        this.auth.updateUser(data,id);
        this.getUser();

    }


}
