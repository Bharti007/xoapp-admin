import { Component } from '@angular/core';
import { AuthService } from '../../auth.service';
@Component({
  templateUrl: 'users.component.html'
})
export class UsersComponent {
 users:any=[]
  constructor(public auth: AuthService) {
    this.auth.getAppUsers('Customer').then(resp => {
      console.log(resp)
      if(resp ){
          this.users = resp;

      }
    },
      error => {
        this.auth.showToastr('error', "Error", "Something Went wrong")
      }
    );
  }


}
