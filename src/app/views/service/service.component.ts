import { Component } from '@angular/core';
import { AuthService } from '../../auth.service';



@Component({
  templateUrl: 'service.component.html'
})
export class ServiceComponent {
 users:any=[]
  constructor(public auth: AuthService) {
    this.auth.getAppUsers('serviceprovider').then(resp => {
      if(resp ){
          this.users = resp;
      }
    },
      error => {
        this.auth.showToastr('error', "Error", "Something Went wrong")
      }
    );
  }


}
