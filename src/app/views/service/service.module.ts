import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceComponent } from './service.component';

//Routing
import { ServiceRoutingModule } from './service-routing.module';

@NgModule({
  imports: [
    ServiceRoutingModule,
    CommonModule
  ],
  declarations: [
    ServiceComponent
  ]
})
export class ServiceModule { }
