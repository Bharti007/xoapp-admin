import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService } from '../../auth.service'

import { ReactiveFormsModule,FormBuilder, FormGroup } from '@angular/forms';
import { Validator } from '../../validator';

@Component({
  templateUrl: 'login.component.html'
})
export class LoginComponent {
 private loginForm: FormGroup;
  constructor(public router: Router, public auth: AuthService,public formBuilder: FormBuilder) {
     this.loginForm = formBuilder.group({
      email: Validator.emailValidator,
      password: Validator.passwordValidator
      
    });
    //  var db =  firebase.firestore();
    //  db.collection('posts').add({'title':"Bharti1","description":"kashyap","image":"djjcdbfjhbjhdbh","createdOn":"67/45/45"}).then((resp)=>{
    //    console.log(resp);
    //  }).catch((error)=>{
    //    console.log(error)
    //  })

    // db.collection('users').onSnapshot((data)=>{
    //   console.log(data);
    // });
    // console.log(firebase.auth())

    // var data = afs.list('/accounts').valueChanges().subscribe(data=>{
    //   console.log(data);
    // })
    // var data1 = this.auth.getAppUsers();
    // console.log(data1);



  }


  btnClick() {
    this.router.navigateByUrl('/pages/register');
  };

  login(){
    this.auth.emailLogin(this.loginForm.value["email"],this.loginForm.value["password"]);
  }

}
