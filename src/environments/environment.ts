// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
      apiKey: "AIzaSyCDaojudyAOTaYoGWfisNv8xPUekNFKwh0",
      authDomain: "xoapp-6fc68.firebaseapp.com",
      databaseURL: "https://xoapp-6fc68.firebaseio.com",
      projectId: "xoapp-6fc68",
      storageBucket: "xoapp-6fc68.appspot.com",
      messagingSenderId: "317204791673"
  },

  fcm_api_key : "key=AAAASdrhAXk:APA91bG6ErEsR6LlyTZqVG_Y5ZgKq0pS1y9SOnxyhk9kKCAjmA2LuwAdoDfQZXM2sXhAazvC-sHEYPa2X5FuOU0c48T4zFjlYLJ7LyNYvhjj3-XKDyREIegsG8PXUhl7Y66t6DDvsRLqgBCuM7oklt08HrsOv7Op_Q" ,
  fcm_api_url:"https://fcm.googleapis.com/fcm/send"
};
